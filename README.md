<p align="center">
  <img src="/logo.png" width="128" height="128"/>
</p>
<h1 align="center">Notiss API client for the Communication API</h1>

## Requirements ##
To use the Notiss Communication API client, the following things are required:
+ Get yourself a Notiss Communication account
+ Get yourself an API key
+ Now you're ready to interact with the Notiss Communication API

## Composer Installation ##
By far the easiest way to install the Notiss Communication API client is to require it with [Composer](http://getcomposer.org/doc/00-intro.md).

    $ composer require notiss/communication

    {
        "require": {
            "notiss/communication": "^1.0"
        }
    }

## Getting started ##

Initializing the Notiss Communication API client, and setting your API key.

```php
$notiss_com = new \Notiss\Communication\ApiClient("API_KEY_HERE","https://communication.notiss.nl/api/v1/connect/");
``` 

### Get domain details ###

```php
$domain_details = $notiss_com->get('domain-details');
```

### Get tags contacts can subscribe to ###

```php
$tags = $notiss_com->tags->get();
```

### Get all tags ###

```php
$tags = $notiss_com->tags->all();
```

### Subscribe to tag ###
You can pass in a contact array and/or a organization.
If the contact and/or organization does not exist, the organization is created and the optin is set to true
If the contact and/or organization is passed to the request, the email or the mobile attribute is required.
In addition is the organization name is also required.
Empty values will be removed from the request, and wont be updated.


```php
$contact = array(
    'title' => '',
    'firstname' => '',
    'lastname' => '',
    'gender' => '',
    'address' => '',
    'address2' => '',
    'zipcode' => '',
    'city' => '',
    'country' => '',
    'phone' => '',
    'mobile' => '',
    'email' => 'info@notiss.nl',
    'language' => '',
);

$organization = array(
    'creditor_nr' => '',
    'debitor_nr' => '',
    'name' => 'Notiss',
    'address' => '',
    'address2' => '',
    'zipcode' => '',
    'city' => '',
    'country' => '',
    'phone' => '',
    'mobile' => '',
    'fax' => '',
    'email' => 'info@notiss.nl',
    'website' => '',
    'coc' => '',
    'vat_number' => '',
    'iban_name' => '',
    'iban_number' => '',
    'swift_code' => '',
    'language' => '',
);

$data = array(
    'contact' => $contact,
    'organization' => $organization,
);
$tags = $notiss_com->tags->subscirbeToTag($tag_id, $data);
```

## Contact ##

### Get all contacts ###
```php
$notiss_com->contacts->all();
```

### Get contact ###
```php
$notiss_com->contacts->get($contact_id);
```

### Create contact ###
```php
$new_contact = array(
    'title' => '',
    'firstname' => '',
    'lastname' => '',
    'gender' => '',
    'address' => '',
    'address2' => '',
    'zipcode' => '',
    'city' => '',
    'country' => '',
    'phone' => '',
    'mobile' => '',
    'email' => 'info@notiss.nl',
    'language' => '',
);
$notiss_com->contacts->create($new_contact);
```
### Update contact ###
```php
$contact_id = 1;
$contact = array(
    'title' => '',
    'firstname' => '',
    'lastname' => '',
    'gender' => '',
    'address' => '',
    'address2' => '',
    'zipcode' => '',
    'city' => '',
    'country' => '',
    'phone' => '',
    'mobile' => '',
    'email' => 'info@notiss.nl',
    'language' => '',
);
$notiss_com->contacts->update($contact_id, $contact);
```
### Delete contact ###
```php
$notiss_com->contacts->delete($contact_id);
```

## Organization ##

### Get all organizations ###
```php
$notiss_com->organizations->all();
```

### Get organization ###
```php
$notiss_com->organizations->get($organization_id);
```

### Create organization ###
```php
$new_organization = array(
    'title' => '',
    'firstname' => '',
    'lastname' => '',
    'gender' => '',
    'address' => '',
    'address2' => '',
    'zipcode' => '',
    'city' => '',
    'country' => '',
    'phone' => '',
    'mobile' => '',
    'email' => 'info@notiss.nl',
    'language' => '',
);
$notiss_com->organizations->create($new_organization);
```
### Update organization ###
```php
$organization_id = 1;
$organization = array(
    'title' => '',
    'firstname' => '',
    'lastname' => '',
    'gender' => '',
    'address' => '',
    'address2' => '',
    'zipcode' => '',
    'city' => '',
    'country' => '',
    'phone' => '',
    'mobile' => '',
    'email' => 'info@notiss.nl',
    'language' => '',
);
$notiss_com->organizations->update($organization_id, $organization);
```
### Delete organization ###
```php
$notiss_com->organizations->delete($organization_id);
```

### Subscribing to tags with overwrite ###
Does the same as the Subscribe to tag endpoint, empty values will clear contact or organizations attributes if set.


## Support ##
Contact: [www.notiss.nl](https://www.notiss.nl) — development@notiss.nl — +31 85 40 11 397