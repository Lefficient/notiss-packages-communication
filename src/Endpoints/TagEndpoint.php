<?php

	namespace Notiss\Communication\Endpoints;

	class TagEndpoint extends EndpointAbstract
	{
		public function get()
		{
			return $this->client->get('tags');
		}


		public function all()
		{
			return $this->client->get('tags/all');
		}

		public function subscribeToTag($tag_id, $data)
		{

			if(isset($data['contact'])){
				foreach($data['contact'] as $k => $value){
					if(empty($value)){
						unset($data['contact'][$k]);
					}
				}
			}

			if(isset($data['organization'])){
				foreach($data['organization'] as $k => $value){
					if(empty($value)){
						unset($data['organization'][$k]);
					}
				}
			}

			return $this->client->post('tags/'.$tag_id, $data);
		}

		public function subscribeToTagWithOverwrite($tag_id, $data)
		{
			return $this->client->post('tags/'.$tag_id, $data);
		}
	}
?>