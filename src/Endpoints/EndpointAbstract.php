<?php

namespace Notiss\Communication\Endpoints;

use Notiss\Communication\NotissCommunicationApiClient;

abstract class EndpointAbstract
{
	/**
     * @var NotissCommunicationApiClient
     */
    protected $client;

    /**
     * @param NotissCommunicationApiClient $api
     */
    public function __construct(NotissCommunicationApiClient $api)
    {
        $this->client = $api;
    }
}