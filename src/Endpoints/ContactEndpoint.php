<?php

	namespace Notiss\Communication\Endpoints;

	class ContactEndpoint extends EndpointAbstract
	{
		public function all()
		{
			return $this->client->get('contacts');
		}

		public function get($contact_id)
		{
			return $this->client->get('contacts/'.$contact_id);
		}

		public function create($data)
		{
			return $this->client->post('contacts', $data);
		}

		public function update($contact_id, $data)
		{
			return $this->client->put('contacts/'.$contact_id, $data);
		}

		public function delete($contact_id)
		{
			return $this->client->delete('contacts/'.$contact_id);
		}
	}
?>