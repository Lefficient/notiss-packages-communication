<?php

	namespace Notiss\Communication\Endpoints;

	class OrganizationEndpoint extends EndpointAbstract
	{
		public function all()
		{
			return $this->client->get('organizations');
		}

		public function get($organization_id)
		{
			return $this->client->get('organizations/'.$organization_id);
		}

		public function create($data)
		{
			return $this->client->post('organizations', $data);
		}

		public function update($organization_id, $data)
		{
			return $this->client->put('organizations/'.$organization_id, $data);
		}

		public function delete($organization_id)
		{
			return $this->client->delete('organizations/'.$organization_id);
		}
	}
?>