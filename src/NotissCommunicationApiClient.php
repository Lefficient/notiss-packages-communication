<?php
namespace Notiss\Communication;

use GuzzleHttp\Client;
use Notiss\Communication\Endpoints\TagEndpoint;
use Notiss\Communication\Endpoints\ContactEndpoint;
use Notiss\Communication\Endpoints\OrganizationEndpoint;

class NotissCommunicationApiClient
{
	/**
     * Version of our client.
     */
    public const CLIENT_VERSION = "1.0.0";

    /**
     * Endpoint of the remote API.
     */
    public $api_endpoint;

    /**
     * RESTful Tags resource.
     *
     * @var TagEndpoint
     */
    public $tags;

    public $client;

    /**
     * @var string
     */
    protected $apiKey;

    public function __construct($apiKey, $apiBaseUrl)
    {

        $this->setApiKey($apiKey);
    	
        $this->client = new Client([
            'base_uri' => $apiBaseUrl,
            'headers' => [ 'Authorization' => 'Bearer ' . $this->apiKey ]
        ]);
        
        $this->initializeEndpoints();

    }

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function initializeEndpoints()
    {
        $this->tags = new TagEndpoint($this);
        $this->contacts = new ContactEndpoint($this);
        $this->organizations = new OrganizationEndpoint($this);
    }

    public function get($endpoint)
    {
        $raw_response = $this->client->get($endpoint);
        return json_decode($raw_response->getBody()->getContents());
    }

    public function post($endpoint, $data)
    {
        $raw_response = $this->client->request('POST',$endpoint, [
            'form_params' => $data,
        ]);

        return json_decode($raw_response->getBody()->getContents());
    }

    public function put($endpoint, $data)
    {
        $raw_response = $this->client->request('PUT',$endpoint, [
            'form_params' => $data,
        ]);

        return json_decode($raw_response->getBody()->getContents());
    }

    public function delete($endpoint)
    {
        $raw_response = $this->client->delete($endpoint);
        return json_decode($raw_response->getBody()->getContents());
    }
}

?>